import { Injectable } from '@angular/core';
    import { Observable, Subject } from 'rxjs';
    
    @Injectable({ providedIn: 'root' })
    export class CommonService {
        private subjectName = new Subject<any>();
        private subjectFilter = new Subject<any>();
        private subjectTimegraph = new Subject<any>();
        private subjectShowFiltering = new Subject<any>();
    
        sendUpdate() {
            this.subjectName.next({ text: "refresh" });
        }

        sendFilter() {
            this.subjectFilter.next({ text: "filters" });
        }

        sendReloadTimegraph() {
            this.subjectTimegraph.next({ text: "timegraph" });
        }

        sendShowFiltering() {
            this.subjectShowFiltering.next({ text: "show" });
        }
    
        getUpdate(): Observable<any> {
            return this.subjectName.asObservable();
        }

        getFilter(): Observable<any> {
            return this.subjectFilter.asObservable();
        }

        getReloadTimegraph(): Observable<any> {
            return this.subjectTimegraph.asObservable();
        }

        getShowFiltering(): Observable<any> {
            return this.subjectShowFiltering.asObservable();
        }
    }