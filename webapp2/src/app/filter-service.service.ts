import {Injectable} from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {environment} from "../environments/environment";
import {HttpClient} from '@angular/common/http';
import {Router} from "@angular/router";
import {MatDialog} from '@angular/material';
import {WaitingCircleComponentComponent} from './real-ws/waiting-circle-component/waiting-circle-component.component';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'app/common-service.service';

@Injectable({
  providedIn: 'root'
})
export class FilterServiceService{
  filtersPerProcess : any;
  filtersPerProcessLayout : any;
  thisProcess : string;
  webservicePath: string;

  constructor(private http: HttpClient, private router : Router, public dialog: MatDialog, private toastr: ToastrService, private service: CommonService) {
    this.webservicePath = environment.webServicePath;
    this.retrieveFiltersFromLocalStorage();
  }

  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0,
          v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  retrieveFiltersFromLocalStorage() {
    this.filtersPerProcess = localStorage.getItem("filtersPerProcess");
    this.filtersPerProcessLayout = localStorage.getItem("filtersPerProcessLayout");
    if (this.filtersPerProcess == null) {
      this.filtersPerProcess = new Object();
      this.filtersPerProcessLayout = new Object();
    }
    else {
      this.filtersPerProcess = JSON.parse(this.filtersPerProcess);
      this.filtersPerProcessLayout = JSON.parse(this.filtersPerProcessLayout);
    }
    this.thisProcess = localStorage.getItem("process");
    if (!(this.thisProcess in this.filtersPerProcess)) {
      this.filtersPerProcess[this.thisProcess] = [];
      this.filtersPerProcessLayout[this.thisProcess] = [];
    }
  }

  addFilter(filter_type : string, filter_value : any, noElements : boolean) {

    var canProceed : any  = localStorage.getItem("canProceed");

    canProceed = JSON.parse(canProceed);

    if (canProceed) {
      return
    }

    let httpParams : HttpParams = new HttpParams();
    httpParams = httpParams.set("uniqueCallId", this.newGuid());

    this.thisProcess  = localStorage.getItem("process");
    var temporaryFilters : any  = localStorage.getItem("temporaryFilters");

    if(temporaryFilters == null) {
      
      temporaryFilters = {
        "nFilters": 0,
        "filtersPerProcessLayout": [],
        "filtersPerProcess": []
      };

    }
    else {
      temporaryFilters = JSON.parse(temporaryFilters)
    }

    temporaryFilters['nFilters']++;

    if(!noElements) {
      temporaryFilters['filtersPerProcessLayout'].push([filter_type.split('-')[0], filter_value])
      temporaryFilters['filtersPerProcess'].push([filter_type, filter_value])
    }

    localStorage.setItem("temporaryFilters", JSON.stringify(temporaryFilters));

    if(temporaryFilters.nFilters < 11) {
      this.dialog.closeAll();
      return;
    }

    localStorage.setItem("canProceed", JSON.stringify(true));

    if (this.filtersPerProcess == null) {
      this.filtersPerProcess = new Object();
    }
    
    if (!(this.thisProcess in this.filtersPerProcess)) {
      this.filtersPerProcess[this.thisProcess] = [];
    }

    if(this.filtersPerProcess[this.thisProcess].length > 0) {

      this.filtersPerProcess[this.thisProcess] = [];
      this.filtersPerProcessLayout[this.thisProcess] = [];
      localStorage.setItem("filtersPerProcess", JSON.stringify(this.filtersPerProcess));
      localStorage.setItem("filtersPerProcessLayout", JSON.stringify(this.filtersPerProcessLayout));
  
      let removeHttpParams : HttpParams = new HttpParams();
      removeHttpParams = removeHttpParams.set("uniqueCallId", this.newGuid());
  
      this.removeAllFiltesrPOST(null, this.filtersPerProcess[this.thisProcess], removeHttpParams).subscribe(data => {
  
        this.callAddFilter(temporaryFilters, filter_type, httpParams)
      })
    }
    else {
      this.callAddFilter(temporaryFilters, filter_type, httpParams)
    }
  }

  callAddFilter(temporaryFilters, filter_type, httpParams) {

    this.filtersPerProcess[this.thisProcess] = temporaryFilters.filtersPerProcess;
    this.filtersPerProcessLayout[this.thisProcess] = temporaryFilters.filtersPerProcessLayout;
    localStorage.setItem("filtersPerProcess", JSON.stringify(this.filtersPerProcess));
    localStorage.setItem("filtersPerProcessLayout", JSON.stringify(this.filtersPerProcessLayout));

    this.addFilterPOST(this.filtersPerProcessLayout[this.thisProcess], httpParams).subscribe(data => {

      localStorage.removeItem("temporaryFilters");

      this.sendMessage();

      if (filter_type == "timestamp_trace_containing") {
        this.service.sendReloadTimegraph();
      }
    })
  }

  sendMessage(): void {
    this.service.sendUpdate();
}

  remove(filter, newFilters = false) {
    
    let httpParams : HttpParams = new HttpParams();
    httpParams = httpParams.set("uniqueCallId", this.newGuid());
    this.dialog.open(WaitingCircleComponentComponent);

    if (filter == null) {
      this.filtersPerProcess[this.thisProcess] = [];
      this.filtersPerProcessLayout[this.thisProcess] = [];
      localStorage.setItem("filtersPerProcess", JSON.stringify(this.filtersPerProcess));
      localStorage.setItem("filtersPerProcessLayout", JSON.stringify(this.filtersPerProcessLayout));

      this.removeAllFiltesrPOST(filter, this.filtersPerProcess[this.thisProcess], httpParams).subscribe(data => {
        if (!newFilters) {
          this.dialog.closeAll();

          if (this.router.url === "/real-ws/dashboard") {
            this.router.navigateByUrl("/real-ws/dashboard2");
          }
          else {
            this.router.navigateByUrl("/real-ws/dashboard");
          }

          this.toastr.success('Deleted Successfully', 'All filters removed', {
            timeOut: 3000,
          });
        }
      })

      return;
    }

    let thisIndex : number = this.filtersPerProcess[this.thisProcess].indexOf(filter, 0);
    this.filtersPerProcess[this.thisProcess].splice(thisIndex, 1);
    this.filtersPerProcessLayout[this.thisProcess].splice(thisIndex, 1);
    localStorage.setItem("filtersPerProcess", JSON.stringify(this.filtersPerProcess));
    localStorage.setItem("filtersPerProcessLayout", JSON.stringify(this.filtersPerProcessLayout));

    let filter1 = [filter[0].split('-')[0], filter[1]]

    this.removeFilterPOST(filter1, this.filtersPerProcessLayout[this.thisProcess], httpParams).subscribe(data => {
      
      this.dialog.closeAll();

      if (this.router.url === "/real-ws/dashboard") {
        this.router.navigateByUrl("/real-ws/dashboard2");
      }
      else {
        this.router.navigateByUrl("/real-ws/dashboard");
      }
      
      this.toastr.success('Deleted Successfully', 'Filter removed', {
        timeOut: 3000,
      });
    })

  }

  getFilters() {
    this.retrieveFiltersFromLocalStorage();
    return [this.filtersPerProcess[this.thisProcess], this.filtersPerProcessLayout[this.thisProcess]];
  }

  addFilterPOST(all_filters : any, parameters : HttpParams) {

    let process = localStorage.getItem("process");
    let sessionId = localStorage.getItem("sessionId");

    parameters = parameters.set("process", process);
    parameters = parameters.set("session", sessionId);

    var completeUrl: string = this.webservicePath + "addFilter";

    return this.http.post(completeUrl, {"all_filters": all_filters}, {params: parameters});
  }

  removeFilterPOST(filter : any, all_filters : any, parameters : HttpParams) {
    var filter_dictio = {"filter": filter, "all_filters": all_filters};

    let process = localStorage.getItem("process");
    let sessionId = localStorage.getItem("sessionId");

    parameters = parameters.set("process", process);
    parameters = parameters.set("session", sessionId);

    var completeUrl: string = this.webservicePath + "removeFilter";

    return this.http.post(completeUrl, filter_dictio, {params: parameters});
  }

  removeAllFiltesrPOST(filter : any, all_filters : any, parameters : HttpParams) {
    var filter_dictio = {"filter": filter, "all_filters": {}};

    let process = localStorage.getItem("process");
    let sessionId = localStorage.getItem("sessionId");

    parameters = parameters.set("process", process);
    parameters = parameters.set("session", sessionId);

    var completeUrl: string = this.webservicePath + "removeFilter";

    return this.http.post(completeUrl, filter_dictio, {params: parameters});
  }
}
