import { Component, Output, EventEmitter, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../services/layout.service';
import { Subscription } from 'rxjs';
import { ConfigService } from '../services/config.service';
import {AuthenticationServiceService} from '../../authentication-service.service';
import {Router, RoutesRecognized} from '@angular/router';
import {HttpParams} from '@angular/common/http';
import {Pm4pyService} from '../../pm4py-service.service';
import {LogSharingComponent} from '../../real-ws/log-sharing/log-sharing.component';
import {FilterServiceService} from '../../filter-service.service';

import {MatDialog, MatIcon} from '@angular/material';

import {StartActivitiesFilterComponent} from "../../real-ws/start-activities-filter/start-activities-filter.component";
import {EndActivitiesFilterComponent} from "../../real-ws/end-activities-filter/end-activities-filter.component";
import {VariantsFilterComponent} from "../../real-ws/variants-filter/variants-filter.component";
import {AttributesFilterComponent} from "../../real-ws/attributes-filter/attributes-filter.component";
import {TimeframeFilterComponent} from '../../real-ws/timeframe-filter/timeframe-filter.component';
import {PerformanceFilterComponent} from '../../real-ws/performance-filter/performance-filter.component';
import {NumericAttributeFilterComponent} from '../../real-ws/numeric-attribute-filter/numeric-attribute-filter.component';
import {WaitingCircleComponentComponent} from '../../real-ws/waiting-circle-component/waiting-circle-component.component';

import {PathsFilterComponent} from '../../real-ws/paths-filter/paths-filter.component';
import {environment} from '../../../environments/environment';
import { CommonService } from 'app/common-service.service';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"]
})
export class NavbarComponent implements OnInit, AfterViewInit, OnDestroy {
  currentLang = "en";
  toggleClass = "ft-maximize";
  placement = "bottom-right";
  public isCollapsed = true;
  layoutSub: Subscription;
  @Output()
  toggleHideSidebar = new EventEmitter<Object>();

  public filters : any;

  public config: any = {};

  public enableSmartFiltering : boolean;

  public sessionId : string;
  public userId : string;
  public isNotLogin : boolean;
  public enableDownload : boolean;
  public enableUpload : boolean;
  public thisProcess : string;
  public isAdmin : boolean;
  public isProcessModelPage : boolean;
  public isPlistPage : boolean;
  public enableSharing: boolean = true;

  public dialog : MatDialog;

  public filterCorrespondence : any = {};
  private subscriptionName: Subscription;

  constructor(public translate: TranslateService, private layoutService: LayoutService, private configService:ConfigService, private authService: AuthenticationServiceService, private _route : Router, private pm4pyServ: Pm4pyService, public _dialog: MatDialog, public filterService: FilterServiceService, private service: CommonService) {
    const browserLang: string = translate.getBrowserLang();
    translate.use(browserLang.match(/en|es|pt|de/) ? browserLang : "en");

    this.layoutSub = layoutService.changeEmitted$.subscribe(
      direction => {
        const dir = direction.direction;
        if (dir === "rtl") {
          this.placement = "bottom-left";
        } else if (dir === "ltr") {
          this.placement = "bottom-right";
        }
      });

    this.dialog = _dialog;

    this.getFilters();
    _route.events.subscribe((val) => {
      this.getFilters();
    });

    this.subscriptionName= this.service.getUpdate().subscribe(message => {
      this.getFilters();
  });

    if (localStorage.getItem("smartFiltering") === null) {
      localStorage.setItem("smartFiltering", "true");
    }

    if (localStorage.getItem("isFiltersOpen") === null) {
      localStorage.setItem("isFiltersOpen", "false");
    }

    if (localStorage.getItem("smartFiltering") === "true") {
      this.enableSmartFiltering = true;
    }
    else {
      this.enableSmartFiltering = false;
    }

    this.enableSharing = environment.overallEnableSharing;


    this.sessionId = null;
    this.userId = null;
    this.isNotLogin = false;
    this.enableDownload = false;
    this.enableUpload = true;
    this.thisProcess = null;
    this.isAdmin = false;

    this.isProcessModelPage = false;
    this.isPlistPage = true;

    this.filterCorrespondence["ltl"] = "LTL";
    this.filterCorrespondence["start_activities"] = "Start act.";
    this.filterCorrespondence["end_activities"] = "End act.";
    this.filterCorrespondence["attributes_pos_trace"] = "Attributes";
    this.filterCorrespondence["attributes_neg_trace"] = "Attributes";
    this.filterCorrespondence["attributes_pos_events-0"] = "Activity";
    this.filterCorrespondence["attributes_pos_events-1"] = "Complexity";
    this.filterCorrespondence["attributes_pos_events-2"] = "Subcontracting";
    this.filterCorrespondence["attributes_pos_events-3"] = "Mold Area";
    this.filterCorrespondence["attributes_pos_events-4"] = "Material Type";
    this.filterCorrespondence["attributes_pos_events-5"] = "N. Cavities";
    this.filterCorrespondence["attributes_pos_events-6"] = "Geometry Dimensions";
    this.filterCorrespondence["attributes_pos_events-7"] = "Price";
    this.filterCorrespondence["attributes_pos_events-8"] = "Molde";
    this.filterCorrespondence["attributes_pos_events-9"] = "Part";
    this.filterCorrespondence["paths_pos_trace"] = "Paths";
    this.filterCorrespondence["paths_neg_trace"] = "Paths";
    this.filterCorrespondence["case_performance_filter"] = "Performance";
    this.filterCorrespondence["timestamp_trace_intersecting"] = "Timeframe";
    this.filterCorrespondence["timestamp_trace_containing"] = "Dateframe";
    this.filterCorrespondence["timestamp_events"] = "Timeframe";
    this.filterCorrespondence["variants"] = "Variants";
    this.filterCorrespondence["numeric_attr_traces"] = "Price";
    this.filterCorrespondence["numeric_attr_events-7"] = "Price";
    this.filterCorrespondence["numeric_attr_events-8"] = "Molde";
    this.filterCorrespondence["numeric_attr_events"] = "Price";


    this.authService.checkAuthentication().subscribe(data => {

      this.sessionId = data.sessionId;
      this.userId = data.userId;
      this.isNotLogin = data.isNotLogin;
      this.enableDownload = data.enableDownload;
      this.enableUpload = data.enableUpload;
      this.isAdmin = data.isAdmin;
      this.thisProcess = localStorage.getItem("process");
  });

    this._route.events.subscribe((next) => {
      if (next instanceof RoutesRecognized) {
        this.authService.checkAuthentication().subscribe(data => {

          this.sessionId = data.sessionId;
          this.userId = data.userId;
          this.isNotLogin = data.isNotLogin;
          this.enableDownload = data.enableDownload;
          this.enableUpload = data.enableUpload;
          this.isAdmin = data.isAdmin;
          this.thisProcess = localStorage.getItem("process");
        });

        if (next.url.startsWith("/real-ws/dashboard")) {
          this.isProcessModelPage = true;
          this.isPlistPage = false;
        }
        else if (next.url.startsWith("/real-ws/plist")) {
          this.isProcessModelPage = false;
          this.isPlistPage = true;
        }
        else if (next.url.startsWith("/real-ws") || next.url.startsWith("/pages/login")) {
          this.isProcessModelPage = false;
          this.isPlistPage = false;
        }
      }
    });
  }

  ngOnInit() {
    this.config = this.configService.templateConf;
  }

  ngAfterViewInit() {
    if(this.config.layout.dir) {
      setTimeout(() => {
        const dir = this.config.layout.dir;
        if (dir === "rtl") {
          this.placement = "bottom-left";
        } else if (dir === "ltr") {
          this.placement = "bottom-right";
        }
      }, 0);
     
    }
  }

  ngOnDestroy() {
    if (this.layoutSub) {
      this.layoutSub.unsubscribe();
    }
  }

  public showFilters() {
    this.service.sendShowFiltering();
  }

  public getFilters() {
    this.filters = this.filterService.getFilters()[0];
  }

  ChangeLanguage(language: string) {
    this.translate.use(language);
  }

  ToggleClass() {
    if (this.toggleClass === "ft-maximize") {
      this.toggleClass = "ft-minimize";
    } else {
      this.toggleClass = "ft-maximize";
    }
  }

  toggleNotificationSidebar() {
    this.layoutService.emitNotiSidebarChange(true);
  }

  toggleSidebar() {
    const appSidebar = document.getElementsByClassName("app-sidebar")[0];
    if (appSidebar.classList.contains("hide-sidebar")) {
      this.toggleHideSidebar.emit(false);
    } else {
      this.toggleHideSidebar.emit(true);
    }
  }

  logout() {
    this.authService.doLogout();
  }

  goToHome() {
    this._route.navigateByUrl("/real-ws/plist");
  }

  downloadCSV() {
    let httpParams : HttpParams = new HttpParams();

    this.dialog.open(WaitingCircleComponentComponent);

    this.pm4pyServ.downloadCsvLog(httpParams).subscribe(data => {
      let csvJson : JSON = data as JSON;

      this.dialog.closeAll();

      this.downloadFile(csvJson['content'], 'text/csv', 'csv');
    });
  }

  downloadXES() {
    let httpParams : HttpParams = new HttpParams();

    this.dialog.open(WaitingCircleComponentComponent);

    this.pm4pyServ.downloadXesLog(httpParams).subscribe(data => {
      let xesJson : JSON = data as JSON;

      this.dialog.closeAll();

      this.downloadFile(xesJson['content'], 'text/csv', 'xes');
    });
  }

  downloadFile(data: string, type: string, extension: string) {
    const blob = new Blob([data], { type: type });
    const url= window.URL.createObjectURL(blob);

    var fileLink = document.createElement('a');
    fileLink.href = url;
    fileLink.download = this.thisProcess + '.' + extension;
    fileLink.click();
  }

  uploadFile($event) {
    let reader = new FileReader();
    let filename : string = $event.target.files[0].name;
    let filetype : string = $event.target.files[0].type;
    let extension : string = filename.split(".")[1];
    if (extension === "xes" || extension === "csv") {
      this.dialog.open(WaitingCircleComponentComponent);

      reader.readAsDataURL($event.target.files[0]);
      reader.onload = () => {
        let base64: string = reader.result.toString();
        let data : any = {"filename": filename, "base64": base64};
        this.pm4pyServ.uploadLog(data, new HttpParams()).subscribe(data => {
          let responseJson : JSON = data as JSON;

          this.dialog.closeAll();
          (document.getElementById("inputUploadLog") as HTMLInputElement).value = ""

          if (responseJson["status"] === "OK") {
            if (this._route.url === "/real-ws/plist") {
              this._route.navigateByUrl("/real-ws/plist2");
            }
            else {
              this._route.navigateByUrl("/real-ws/plist");
            }
          }
          else if(responseJson["status"] === "Invalid Log") {

            alert("Something is wrong with the logfile uploaded!");

            if (this._route.url === "/real-ws/plist") {
              this._route.navigateByUrl("/real-ws/plist2");
            }
            else {
              this._route.navigateByUrl("/real-ws/plist");
            }
          }
          else {
            alert("Something has gone wrong in the upload!");

            if (this._route.url === "/real-ws/plist") {
              this._route.navigateByUrl("/real-ws/plist2");
            }
            else {
              this._route.navigateByUrl("/real-ws/plist");
            }
          }
        })
      }
    }
    else {
      alert("unsupported file type for direct upload!")
    }
  }

  /*startActivitiesFilter() {
    this.dialog.open(StartActivitiesFilterComponent);
  }

  endActivitiesFilter() {
    this.dialog.open(EndActivitiesFilterComponent);
  }

  variantsFilter() {
    this.dialog.open(VariantsFilterComponent);
  }*/

  attributesFilter(type: Number, attribute:string, name:string) {
    this.dialog.open(AttributesFilterComponent, {
      data: {
          filter: {
              type: 'attributes_pos_trace-' + type,
              attribute: attribute,
              name: name
          }
      }
   });
  }

  /*pathsFilter() {
    this.dialog.open(PathsFilterComponent);
  }

  performanceFilter() {
    this.dialog.open(PerformanceFilterComponent);
  }*/

  timeframeFilter() {
    this.dialog.open(TimeframeFilterComponent);
  }

  numericAttributeFilter() {
    this.dialog.open(NumericAttributeFilterComponent);
  }

  /*shareLog() {
      this.dialog.open(LogSharingComponent);
  }*/

  changeSmartFiltering() {
    if (this.enableSmartFiltering) {
      this.enableSmartFiltering = false;
      localStorage.setItem("smartFiltering", "false");
      alert("Smart filtering has been disabled for the next calculations.");
    }
    else {
      this.enableSmartFiltering = true;
      localStorage.setItem("smartFiltering", "true");
      alert("Smart filtering has been enabled for the next calculations.");
    }
  }

  tooltipValue(filter: any){
    switch(filter[0].split("-")[0]){
      case "attributes_pos_trace":
      case "attributes_pos_events":
        return filter[1][1].toString().replaceAll(',', ' | ')
      break;
      case "numeric_attr_events":
        let max : Number = Number(filter[1][1].split("@@@")[1])
        let min : Number = Number(filter[1][1].split("@@@")[0])
        let tooltip = '';

        if(min != Number.MIN_SAFE_INTEGER){
          tooltip = "Min: " + min;
        }

        if(min != Number.MIN_SAFE_INTEGER && max != Number.MAX_SAFE_INTEGER){
          tooltip += " | ";
        }

        if(max != Number.MAX_SAFE_INTEGER){
          tooltip += "Max: " + max;
        }

        return tooltip;
        break;
        case "timestamp_trace_containing":
          let min_date = this.timeConverter(filter[1].split('@@@')[0]);
          let max_date = this.timeConverter(filter[1].split('@@@')[1]);

          return "From: " + min_date + " | To: " + max_date;

          break;
    }
    
  }

  timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = String(a.getDate());
 
    if (date.length < 2) {
      date = "0" + date;
    }
 
    return year + "/" + month + "/" + date
  }
}
