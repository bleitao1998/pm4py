import { Component, OnInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {Pm4pyService} from "../../pm4py-service.service";
import {HttpParams} from "@angular/common/http";
import {AuthenticationServiceService} from '../../authentication-service.service';
import {WaitingCircleComponentComponent} from '../waiting-circle-component/waiting-circle-component.component';
import {MatDialog} from '@angular/material';
import { graphviz } from 'd3-graphviz';

@Component({
  selector: 'app-transient',
  templateUrl: './transient.component.html',
  styleUrls: ['./transient.component.scss']
})
export class TransientComponent implements OnInit {
  public isLoading: boolean;
  processModelBase64Original: string;
  processModelBase64Sanitized: SafeResourceUrl;
  pm4pyJson: JSON;
  public delay: number = 11.36675;
  public selectedDelay : number = 11.36675;
  public expSelectedDelay : number = Math.exp(this.selectedDelay);
  public shownString : string = this.secondsToString(this.expSelectedDelay);
  sanitizer: DomSanitizer;
  pm4pyService: Pm4pyService;
  targetWidth : number;
  targetHeight : number;

  constructor(private _sanitizer: DomSanitizer, private pm4pyServ: Pm4pyService, private authService: AuthenticationServiceService, public dialog: MatDialog) {
    this.sanitizer = _sanitizer;
    this.pm4pyService = pm4pyServ;

    this.authService.checkAuthentication().subscribe(data => {
    });

    // calls the retrieval of the simulation from the service
    this.populateSimulation();
  }

  ngOnInit() {
  }

  secondsToString(seconds : number) {
    let numdays : number = Math.floor(seconds / 86400);
    let numhours : number = Math.floor((seconds % 86400) / 3600);
    let numminutes : number = Math.floor(((seconds % 86400) % 3600) / 60);
    let numseconds : number = Math.floor(((seconds % 86400) % 3600) % 60);

    if (numdays >= 1) {
      return numdays.toString() + "D " + numhours.toString() + "h";
    }
    else if (numhours >= 1) {
      return  numhours.toString() + "h " + numminutes.toString()+"m";
    }
    else if (numminutes >= 1) {
      return numminutes.toString() + "m " + numseconds.toString()+"s";
    }
    else if (numseconds >= 1) {
      return numseconds.toString()+"s";
    }
    return "0";
  }

  populateSimulation() {
    this.isLoading = true;
    let params: HttpParams = new HttpParams();
    params = params.set("delay", this.expSelectedDelay.toString());

    this.dialog.open(WaitingCircleComponentComponent);


    this.pm4pyService.transientAnalysis(params).subscribe(data => {
      this.pm4pyJson = data as JSON;
      this.processModelBase64Original = this.pm4pyJson["gviz_base64"];
      //this.processModelBase64Sanitized = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/svg+xml;base64,' + this.processModelBase64Original);
      this.isLoading = false;

      if (this.processModelBase64Original == null || typeof (this.processModelBase64Original) == "undefined") {
        this.processModelBase64Original = "";
      }

      if (this.processModelBase64Original.length > 0) {
        this.processModelBase64Original = atob(this.processModelBase64Original);
      }

      if(this.processModelBase64Original.length > 0 && this.processModelBase64Original.length > 0) {

        this.targetWidth = window.innerWidth * 0.90;
        this.targetHeight = window.innerHeight * 0.68;

        graphviz('#imageProcessModelImage').width(this.targetWidth).height(this.targetHeight).renderDot(this.processModelBase64Original);
        let imageProcessModelImage = document.getElementById("imageProcessModelImage");
        let svgDoc = imageProcessModelImage.childNodes;
        (<SVGSVGElement>svgDoc[0]).currentScale = 1;
      }

      this.dialog.closeAll();
    });
  }

  sliderIsChanged(event: any) {
    /**
     * Manages the change to the value selected in the slider
     */
    this.selectedDelay = event.value;
    this.expSelectedDelay  = Math.exp(this.selectedDelay);
    this.shownString = this.secondsToString(this.expSelectedDelay);
    // calls the retrieval of the process schema from the service
    this.populateSimulation();
  }

  manualInputManagement(event) {
    
    
    let noDays : number = parseFloat((<HTMLInputElement> document.getElementById("numberDays")).value);
    
    let noSeconds : number = noDays * 86400.0;
    this.delay = Math.log(noSeconds);
    this.selectedDelay = Math.log(noSeconds);
    this.expSelectedDelay = noSeconds;
    this.shownString = this.secondsToString(this.expSelectedDelay);
    this.populateSimulation();
  }

}
