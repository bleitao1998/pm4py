import { Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {HttpParams} from "@angular/common/http";
import {DomSanitizer, SafeResourceUrl} from "@angular/platform-browser";
import {Pm4pyService} from "../../pm4py-service.service";
import {AuthenticationServiceService} from '../../authentication-service.service';
import {MatButton, MatDialog, MatTab, MatTabGroup} from '@angular/material';
import {WaitingCircleComponentComponent} from '../waiting-circle-component/waiting-circle-component.component';
import { graphviz } from 'd3-graphviz';

@Component({
  selector: 'app-alignments',
  templateUrl: './alignments.component.html',
  styleUrls: ['./alignments.component.scss']
})
export class AlignmentsComponent implements OnInit {

  sanitizer: DomSanitizer;
  pm4pyService: Pm4pyService;
  public isLoading : boolean = true;
  pm4pyJson: JSON;
  projectionImage: SafeResourceUrl;
  tableImage: SafeResourceUrl;
  projectionString : string;
  tableString : string;
  targetWidth : number;
  targetHeight : number;
  divHidden : boolean = false;

  constructor(private _sanitizer: DomSanitizer, private pm4pyServ: Pm4pyService, private authService: AuthenticationServiceService, public dialog: MatDialog) {
    this.pm4pyService = pm4pyServ;
    this.sanitizer = _sanitizer;

    this.authService.checkAuthentication().subscribe(data => {
    });

    this.populateVisualizations();
  }

  ngOnInit() {
  }

  public populateVisualizations() {
    let model : string = localStorage.getItem("process_model");
    let modelType : string = localStorage.getItem("preferred_type_of_model");

    let params : HttpParams = new HttpParams();

    let waitingCircle = this.dialog.open(WaitingCircleComponentComponent);

    this.pm4pyService.getAlignmentsVisualizations(model, modelType, params).subscribe(data => {
      this.pm4pyJson = data as JSON;
      
      this.projectionString = this.pm4pyJson["petri"];
      this.tableString = this.pm4pyJson["table"];

      if (this.projectionString == null || typeof (this.projectionString) == "undefined") {
        this.projectionString = "";
      }

      if (this.projectionString.length > 0) {
        this.projectionString = atob(this.projectionString);
      }

      if (this.tableString == null || typeof (this.tableString) == "undefined") {
        this.tableString = "";
      }

      if (this.tableString.length > 0) {
        this.tableString = atob(this.tableString);
      }

      this.isLoading = false;

      if(this.tableString.length > 0) {

        this.targetWidth = window.innerWidth * 0.90;
        this.targetHeight = window.innerHeight * 0.68;

        
        graphviz('#tableImageImg').width(this.targetWidth).height(this.targetHeight).renderDot(this.tableString);
        let tableImageImg = document.getElementById("tableImageImg");
        let svgDocTable = tableImageImg.childNodes;
        (<SVGSVGElement>svgDocTable[0]).currentScale = 1;
      }

      if(this.projectionString.length > 0) {

        this.targetWidth = window.innerWidth * 0.90;
        this.targetHeight = window.innerHeight * 0.68;

        graphviz('#projectionImageImg').width(this.targetWidth).height(this.targetHeight).renderDot(this.projectionString);
        let projectionImageImg = document.getElementById("projectionImageImg");
        let svgDoc = projectionImageImg.childNodes;
        (<SVGSVGElement>svgDoc[0]).currentScale = 1;
      }

      waitingCircle.close();
    });
  }

  public changeView(select : boolean, btnProj : MatButton, btnTable : MatButton) {
    if(select == this.divHidden) {
      return;
    }

    if(select) {
      btnProj.color = null
      btnTable.color = "primary"
    }
    else {
      btnProj.color = "primary"
      btnTable.color = null
    }

    this.divHidden = select
  }

}
