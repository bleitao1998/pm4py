import { Component, Inject, Input, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {Pm4pyService} from "../../pm4py-service.service";
import {HttpParams} from "@angular/common/http";
import { FilterServiceService } from '../../filter-service.service';
import {WaitingCircleComponentComponent} from '../waiting-circle-component/waiting-circle-component.component';
import {MatDialog, MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { CommonService } from 'app/common-service.service';

interface AtributesElement {
  attributes: string;
  occurrences: number;
}

@Component({
  selector: 'app-attributes-filter',
  templateUrl: './attributes-filter.component.html',
  styleUrls: ['./attributes-filter.component.scss']
})

export class AttributesFilterComponent implements OnInit, AfterContentInit, OnDestroy {
  @Input('type') type: Number;
  @Input('name') name: string;
  @Input('attribute') attribute: string;

  sanitizer: DomSanitizer;
  pm4pyService: Pm4pyService;
  public selectedAttribute : string;
  public attributeValues : AtributesElement[] = [];
  public selectedAttributeValues : string[];
  public filteringMethod : string;
  private subscriptionName: Subscription;

  displayedColumns: string[] = ['select', 'attributes', 'occurrences'];
  dataSource = new MatTableDataSource(this.attributeValues);
  selection = new SelectionModel<AtributesElement>(true, []);

  constructor(private _sanitizer: DomSanitizer, private pm4pyServ: Pm4pyService, public filterService : FilterServiceService, public dialog: MatDialog, private service: CommonService/*,  @Inject(MAT_DIALOG_DATA) public data: any*/) {
    this.sanitizer = _sanitizer;
    this.pm4pyService = pm4pyServ;
    //this.selectedAttribute = data.filter.attribute;
    //this.filteringMethod = data.filter.type;
    this.filterService = filterService;
    //this.data = data;

    this.subscriptionName= this.service.getFilter().subscribe(message => {
      this.applyFilterToProcess();
    });
    
  }

  ngAfterContentInit() {
    this.selectedAttribute = this.attribute;
    this.filteringMethod = 'attributes_pos_events-' + this.type;
    this.getAttributeValues();
  }
 
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    let process = localStorage.getItem("process");
    let activity = JSON.parse(localStorage.getItem(process + 'activity'));

    if (activity != null) {

      let cavities = JSON.parse(localStorage.getItem(process + 'cavities'));
      let complexity = JSON.parse(localStorage.getItem(process + 'complexity'));
      let subcontracting = JSON.parse(localStorage.getItem(process + 'subcontracting'));
      let molde_area = JSON.parse(localStorage.getItem(process + 'molde_area'));
      let material_type = JSON.parse(localStorage.getItem(process + 'material_type'));
      let geometry = JSON.parse(localStorage.getItem(process + 'geometry'));

      let array = [];

      array.push({'cavities': cavities})
      array.push({'complexity': complexity})
      array.push({'subcontracting': subcontracting})
      array.push({'molde_area': molde_area})
      array.push({'material_type': material_type})
      array.push({'geometry': geometry})
      array.push({'activity': activity})

      let attributes : any = new Object();
      attributes[process] = array;

      localStorage.setItem(process + '-attributesValues', JSON.stringify(attributes))

      localStorage.removeItem(process + 'cavities')
      localStorage.removeItem(process + 'complexity')
      localStorage.removeItem(process + 'subcontracting')
      localStorage.removeItem(process + 'molde_area')
      localStorage.removeItem(process + 'material_type')
      localStorage.removeItem(process + 'geometry')
      localStorage.removeItem(process + 'activity')
    }
  }

  ngOnDestroy(){ 
    this.subscriptionName.unsubscribe();
  }

  getAttributeValues() {
    let process = localStorage.getItem("process");
    let httpParams: HttpParams = new HttpParams();

    let attributes = JSON.parse(localStorage.getItem(process + '-attributesValues'));

    if (attributes == null || attributes[process] == null) {

      this.pm4pyService.getAttributeValues(this.selectedAttribute, httpParams).subscribe(data => {
        let attributeValuesJSON = data as JSON;

        attributeValuesJSON["attributeValues"].forEach(element => {

          this.attributeValues.push({attributes: element[0], occurrences: element[1]})

        });

        this.dataSource = new MatTableDataSource(this.attributeValues);
        this.selection = new SelectionModel<AtributesElement>(true, []);

        this.addFiltersHistory();

        localStorage.setItem(process + this.attribute, JSON.stringify(this.attributeValues));
      });
    }
    else {
      attributes[process].forEach(element => {
        if (element[this.attribute]) {
          element[this.attribute].forEach(element1 => {
            this.attributeValues.push({ attributes: element1.attributes, occurrences: element1.occurrences })
          });
        }
      });

      this.dataSource = new MatTableDataSource(this.attributeValues);
      this.selection = new SelectionModel<AtributesElement>(true, []);

      this.addFiltersHistory();
    }
  }

  addFiltersHistory(){
    let process = localStorage.getItem("process");
      let filtersPerProcess = JSON.parse(localStorage.getItem("filtersPerProcess"));

      if(filtersPerProcess == null || filtersPerProcess[process] == null || filtersPerProcess[process].length == 0){
        return;
      }

      for (var elem of filtersPerProcess[process]){
        if(elem[0] == this.filteringMethod){
            this.dataSource.data.forEach(row => {
            for (var filter of elem[1][1]){
              if (row.attributes == filter){
                this.selection.select(row)                
              }
            }
          })
          break;
        }
      }
  }

  applyFilterToProcess() {
    
    if(this.selection.selected.length == 0){
      this.filterService.addFilter(this.filteringMethod, [this.selectedAttribute, null], true);
      return;
    }
    
    let selected = []
    this.selection.selected.forEach(element => {
      selected.push(element.attributes)
    });

    this.filterService.addFilter(this.filteringMethod, [this.selectedAttribute, selected], false);
  }

}
