import {Component, OnInit, ViewEncapsulation, Input, ElementRef, ViewChild, OnChanges, OnDestroy, AfterViewInit} from '@angular/core';
import { Pm4pyService } from 'app/pm4py-service.service';
import {MatDialog, MatSidenav, MatAccordion} from '@angular/material';
import { CommonService } from 'app/common-service.service';
import { FilterServiceService } from '../../filter-service.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dynamic-filters',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './dynamic-filters.component.html',
  styleUrls: ['./dynamic-filters.component.scss'],
})

export class DynamicFiltersComponent implements OnInit, OnDestroy {

  @ViewChild('sidenav', {
    read: MatSidenav
  }) sidenav: MatSidenav;

  @ViewChild('accordion', {
    read: MatAccordion
  }) accordion: MatAccordion;

  public opened: boolean;
  public isLoading: boolean;
  private subscriptionName: Subscription;

  filters = [
    { name: 'Activity Filter', icon: 'ft-activity mr-2', attribute: 'activity', type: 0},
    { name: 'Complexity Filter', icon: 'fa fa-list mr-2', attribute: 'complexity', type: 1 },
    { name: 'Subcontracting Filter', icon: 'ft-users mr-2', attribute: 'subcontracting', type: 2 },
    { name: 'Molde Area Filter', icon: 'fa fa-area-chart mr-2', attribute: 'molde_area', type: 3 },
    { name: 'Material Type Filter', icon: 'ft-stop-circle mr-2', attribute: 'material_type', type: 4 },
    { name: 'Cavities Number Filter', icon: 'fa fa-sort-numeric-asc mr-2', attribute: 'cavities', type: 5 },
    { name: 'Geometry Dimentions Filter', icon: 'fa fa-square mr-2', attribute: 'geometry', type: 6 }
  ];

  numericFilters = [
    { name: 'Price Filter', icon: 'fa fa-money mr-2', attribute: 'costs', type: 7},
    { name: 'Molde Filter', icon: 'fa fa-diamond mr-2', attribute: 'case', type: 8 },
    { name: 'Part Filter', icon: 'fa fa-puzzle-piece mr-2', attribute: 'num_peca', type: 9 }
  ];

  constructor(
      private pm4pyService: Pm4pyService, public filterService : FilterServiceService,
      private dialog: MatDialog, private service: CommonService) {
    this.isLoading = false;
    this.opened = false;

    this.filterService = filterService;

    this.subscriptionName= this.service.getShowFiltering().subscribe(message => {
      this.accordion.closeAll();
      this.sidenav.toggle();

      let isFiltersOpen = localStorage.getItem('isFiltersOpen');

      if(isFiltersOpen == "true") {
        isFiltersOpen = "false"
      }
      else {
        isFiltersOpen = "true"
      }

      localStorage.setItem('isFiltersOpen', isFiltersOpen);
    });
  }

  ngOnInit() {
    let isFiltersOpen = localStorage.getItem('isFiltersOpen');

    if(isFiltersOpen == "true") {
      this.sidenav.open();
    }
  }

  ngAfterViewInit() {
    
  }

  ngOnDestroy(){ 
    this.subscriptionName.unsubscribe();
  }

  sendMessage(): void {
    this.service.sendUpdate();
  }

  applyFiltersToProcess(): void {
    this.service.sendFilter();
  }

  clearAllFilters(): void {
    this.accordion.closeAll();

    let process = localStorage.getItem("process");
    let filtersPerProcess = JSON.parse(localStorage.getItem("filtersPerProcess"));
    
    if(filtersPerProcess == null || filtersPerProcess[process].length == 0){
      return;
    }
    
    this.filterService.remove(null);
  }
}