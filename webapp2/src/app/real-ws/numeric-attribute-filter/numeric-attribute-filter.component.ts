import { Component, OnInit, Input, ViewChild, OnDestroy, AfterContentInit } from '@angular/core';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Pm4pyService} from '../../pm4py-service.service';
import {FilterServiceService} from '../../filter-service.service';
import {MatDialog, MatInput, MatRadioButton, MatRadioGroup} from '@angular/material';
import {HttpParams} from '@angular/common/http';
import {WaitingCircleComponentComponent} from '../waiting-circle-component/waiting-circle-component.component';
import { Subscription } from 'rxjs';
import { CommonService } from 'app/common-service.service';

@Component({
  selector: 'app-numeric-attribute-filter',
  templateUrl: './numeric-attribute-filter.component.html',
  styleUrls: ['./numeric-attribute-filter.component.scss']
})
export class NumericAttributeFilterComponent implements OnInit, OnDestroy, AfterContentInit {
  @Input('type') type: Number;
  @Input('name') name: string;
  @Input('attribute') attribute: string;

  @ViewChild('minValue', {
    read: MatInput
  }) minValue: MatInput;

  @ViewChild('maxValue', {
    read: MatInput
  }) maxValue: MatInput;

  @ViewChild('multiByComa', {
    read: MatInput
  }) multiByComa: MatInput;

  @ViewChild('radioButtons', {
    read: MatRadioGroup
  }) radioButtonGroup: MatRadioGroup;

  @ViewChild('interval', {
    read: MatRadioButton
  }) intervalRadioBtn: MatRadioButton;

  @ViewChild('oneByOne', {
    read: MatRadioButton
  }) oneByOneRadioBtn: MatRadioButton;

  sanitizer: DomSanitizer;
  pm4pyService: Pm4pyService;
  public attributesList : string[];
  public selectedAttribute : string;
  points : any;
  numericAttributeSvgOriginal : string;
  numericAttributeSvgSanitized : SafeResourceUrl;
  public filteringMethod : string;
  min_value : number;
  max_value : number;
  public selected_min_value : number = Number.MIN_SAFE_INTEGER;
  public selected_max_value : number = Number.MAX_SAFE_INTEGER;
  public is_numeric : boolean;
  private subscriptionName: Subscription;

  constructor(private _sanitizer: DomSanitizer, private pm4pyServ: Pm4pyService, public filterService : FilterServiceService, public dialog: MatDialog, private service: CommonService) {
    this.sanitizer = _sanitizer;
    this.pm4pyService = pm4pyServ;

    this.filterService = filterService;

    this.is_numeric = false;

    this.subscriptionName= this.service.getFilter().subscribe(message => {
      this.applyFilter();
    });

  }

  ngAfterContentInit() {
    this.selectedAttribute = this.attribute;
    this.filteringMethod = 'numeric_attr_events-' + this.type;
    this.addFiltersHistory()
  }
 

  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.subscriptionName.unsubscribe();
  }

  applyFilter() {
    if (this.radioButtonGroup.selected == null) {
      this.filterService.addFilter('attributes_pos_events-'+this.type, [this.selectedAttribute, null], true);
      return
    }
    if (this.radioButtonGroup.selected.value == "interval"){
      //this.filterService.addFilter('attributes_pos_events-'+this.type, [this.selectedAttribute, null], true);

      if(this.minValue.value == '' && this.maxValue.value == ''){
        this.filterService.addFilter(this.filteringMethod, [this.selectedAttribute, String("")+"@@@"+String("")], true);
        return;
      }      
      
      if (this.minValue.value != "")
        this.selected_min_value = parseFloat(this.minValue.value)
      else {
        this.selected_min_value = Number.MIN_SAFE_INTEGER
      }
  
      if (this.maxValue.value != "")
        this.selected_max_value = parseFloat(this.maxValue.value)
      else {
        this.selected_max_value = Number.MAX_SAFE_INTEGER
      }
  
      if (this.selected_min_value > this.selected_max_value){
        let max = this.selected_min_value;
        this.selected_min_value = this.selected_max_value;
        this.selected_max_value = max;
      }
  
      this.filterService.addFilter(this.filteringMethod, [this.selectedAttribute, String(this.selected_min_value)+"@@@"+String(this.selected_max_value)], false);
      return;
    }

    //this.filterService.addFilter(this.filteringMethod, [this.selectedAttribute, String("")+"@@@"+String("")], true);

    if(this.multiByComa.value == ''){
      this.filterService.addFilter('attributes_pos_events-'+this.type, [this.selectedAttribute, null], true);
      return;
    }

    this.filterService.addFilter('attributes_pos_events-'+this.type, [this.selectedAttribute, this.multiByComa.value.replace(/\s/g,'').split(',')], false);

  }

  addFiltersHistory(){
    let process = localStorage.getItem("process");
    let filtersPerProcess = JSON.parse(localStorage.getItem("filtersPerProcess"));

    if(filtersPerProcess == null || filtersPerProcess[process] == null || filtersPerProcess[process].length == 0){
      return;
    }

    var string = ""

    for (var elem of filtersPerProcess[process]){
      if(elem[0] == this.filteringMethod && elem[1][0] == this.attribute){
        this.radioButtonGroup.selected = this.intervalRadioBtn
        this.minValue.value = elem[1][1].split('@@@')[0];
        this.maxValue.value = elem[1][1].split('@@@')[1];
        break;
      }
      else if (elem[0] == 'attributes_pos_events-' + this.type && elem[1][0] == this.attribute) {
        this.radioButtonGroup.selected = this.oneByOneRadioBtn
        string = JSON.stringify(elem[1][1]).replace('[', '').replace(']', '')
        while(string.includes('"')){
          string = string.replace('"', '')
        }
        this.multiByComa.value = string
        break;
      }
    }
  }

  changeParameters(value){

    if(value == "interval"){
      this.multiByComa.readonly = true;
      this.multiByComa.value = "";

      this.maxValue.readonly = false
      this.minValue.readonly = false
    } else {
      this.multiByComa.readonly = false;

      this.maxValue.readonly = true
      this.minValue.readonly = true
      this.minValue.value = "";
      this.maxValue.value = "";
    }

  }

  typeNine(){
    return this.type == 9
  }

}
