import { Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {Pm4pyService} from "../../pm4py-service.service";
import {AuthenticationServiceService} from '../../authentication-service.service';
import { FilterServiceService } from '../../filter-service.service';
import {HttpParams} from '@angular/common/http';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {MatChip, MatDialog, MatDatepicker, MatInput, MatSelect} from '@angular/material';
import {WaitingCircleComponentComponent} from '../waiting-circle-component/waiting-circle-component.component';
import {NgbDate, NgbCalendar, NgbDateParserFormatter, NgbDateStruct, NgbDateAdapter, NgbDatepicker} from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStructAdapter } from '@ng-bootstrap/ng-bootstrap/datepicker/adapters/ngb-date-adapter';
import { CommonService } from 'app/common-service.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
 
@Component({
  selector: 'app-timeframe-filter',
  templateUrl: './timeframe-filter.component.html',
  styleUrls: ['./timeframe-filter.component.scss']
})
export class TimeframeFilterComponent implements OnInit, OnDestroy {
  @ViewChild('d1', {
    read: NgbDatepicker
  }) d1: NgbDatepicker;
 
  @ViewChild('d2', {
    read: NgbDatepicker
  }) d2: NgbDatepicker;
 
  public filteringMethod : string;
  points: any;
  min_timestamp : number;
  max_timestamp : number;
  loaded : boolean;
  eventsPerTimeSvgOriginal: string;
  eventsPerTimeSvgSanitized: SafeResourceUrl;
  private subscriptionName: Subscription;
  private subscriptionTimeframe: Subscription;
 
  model1: string
  model2: string
 
  constructor(private pm4pyServ: Pm4pyService, private authService: AuthenticationServiceService, public filterService : FilterServiceService, private _sanitizer: DomSanitizer, public dialog: MatDialog, private service: CommonService, private calendar: NgbCalendar, private adapter: NgbDateAdapter<string>) {
    this.filteringMethod = "timestamp_trace_containing";
 
    this.loaded = false;
 
    this.authService.checkAuthentication().subscribe(data => {
    });
 
    this.subscriptionName= this.service.getFilter().subscribe(message => {
      this.applyFilter();
    });

    this.subscriptionTimeframe = this.service.getReloadTimegraph().subscribe(message => {
      this.getTimeframeGraph();
    });
  }
 
  ngOnInit() {
    this.getTimeframeGraph();
  }
 
  ngOnDestroy() {
    this.subscriptionName.unsubscribe();
    this.subscriptionTimeframe.unsubscribe();
  }
 
  applyFilter() {

    var hasFilter = false;

    let process = localStorage.getItem("process");
    let filtersPerProcess = JSON.parse(localStorage.getItem("filtersPerProcess"));

    if(filtersPerProcess != null && filtersPerProcess[process] != null) {

      filtersPerProcess[process].forEach(filter =>{
        if(filter[0] == this.filteringMethod){
          hasFilter = true;
        }
      })

    }
 
    var minTempUNIX = this.dateConverter(this.model1['year'] + "-" + this.model1['month'] + "-" + this.model1['day']);
    var maxTempUNIX = this.dateConverter(this.model2['year'] + "-" + this.model2['month'] + "-" + this.model2['day']);

    if ((this.min_timestamp == minTempUNIX && this.max_timestamp == maxTempUNIX) && !hasFilter ){
      this.filterService.addFilter(this.filteringMethod, "", true);
      return;
    } 

    minTempUNIX = this.dateConverter(this.model1['year'] + "-" + this.model1['month'] + "-" + this.model1['day'] + " 00:00:00");
    maxTempUNIX = this.dateConverter(this.model2['year'] + "-" + this.model2['month'] + "-" + this.model2['day'] + " 23:59:59");

    let filterStri = String(minTempUNIX)+"@@@"+String(maxTempUNIX);
    this.filterService.addFilter(this.filteringMethod, filterStri, false);
  }
 
  getTimeframeGraph() {
    
    let params: HttpParams = new HttpParams();

    this.pm4pyServ.getEventsPerTime(params).subscribe(data => {
      let eventsPerTimeJson = data as JSON;
      this.points = eventsPerTimeJson["points"];

      this.eventsPerTimeSvgOriginal = eventsPerTimeJson["base64"];
      this.eventsPerTimeSvgSanitized = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/svg+xml;base64,' + this.eventsPerTimeSvgOriginal);

      if (this.points.length > 0) {
        this.min_timestamp = Math.floor(this.points[0][0]);
        this.max_timestamp = Math.ceil(this.points[this.points.length - 1][0]);
        var min_date = this.timeConverter(this.min_timestamp);
        var max_date = this.timeConverter(this.max_timestamp);

        this.min_timestamp = this.dateConverter(min_date.year + "-" + min_date.month + "-" + min_date.day);
        this.max_timestamp = this.dateConverter(max_date.year + "-" + max_date.month + "-" + max_date.day);
      }

      if (!this.addFiltersHistory()){
        this.model1 = this.adapter.toModel({year: min_date.year, month: min_date.month, day: min_date.day})
        this.model2 = this.adapter.toModel({year: max_date.year, month: max_date.month, day: max_date.day})
        this.d1.navigateTo(new NgbDate(min_date.year, min_date.month, min_date.day))
        this.d2.navigateTo(new NgbDate(max_date.year, max_date.month, max_date.day))
      }
    });
  }

  addFiltersHistory() : boolean {
    let process = localStorage.getItem("process");
    let filtersPerProcess = JSON.parse(localStorage.getItem("filtersPerProcess"));

    var hasFilter = false;

    if(filtersPerProcess == null || filtersPerProcess[process] == null || filtersPerProcess[process].length == 0){
      return hasFilter;
    }


    for (var elem of filtersPerProcess[process]){
      if(elem[0] == this.filteringMethod){
        var min_date = this.timeConverter(elem[1].split('@@@')[0]);
        var max_date = this.timeConverter(elem[1].split('@@@')[1]);
        this.model1 = this.adapter.toModel({year: min_date.year, month: min_date.month, day: min_date.day})
        this.model2 = this.adapter.toModel({year: max_date.year, month: max_date.month, day: max_date.day})
        this.d1.navigateTo(new NgbDate(min_date.year, min_date.month, min_date.day))
        this.d2.navigateTo(new NgbDate(max_date.year, max_date.month, max_date.day))
        hasFilter = true;
        break;
      }
    }

    return hasFilter;
  }
 
  timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = String(a.getDate());
 
    if (date.length < 2) {
      date = "0" + date;
    }
 
    return {year: year, month: Number(month), day: Number(date)};
  }
 
  dateConverter(date_string) {
    var unixTimeZero = Date.parse(date_string) / 1000;
 
    return unixTimeZero;
  }
}
  
