import os
import unittest

from pm4pyws.handlers.xes.xes import XesHandler


def basic_test(path):
    handler = XesHandler()
    handler.build_from_path(path)

class XesTests(unittest.TestCase):
    def test_xes_basic(self):
        basic_test("files/event_logs/running-example.xes")
        basic_test("files/event_logs/receipt.xes")


if __name__ == "__main__":
    unittest.main()
