from pm4py.objects.log.importer.xes import importer as xes_importer
from pm4py.objects.log.util import insert_classifier
from pm4py.objects.log.util import xes
from pm4py.algo.filtering.log.variants import variants_filter
from pm4py.util import constants

from pm4pyws.util import constants as ws_constants

class XesHandler(object):
    def __init__(self):
        """
        Constructor (set all variables to None)
        """

        # sets the current log to None
        self.log = None
        # sets the first ancestor (in the filtering chain) to None
        self.first_ancestor = self
        # sets the last ancestor (in the filtering chain) to None
        self.last_ancestor = self
        # sets the filter chain
        self.filters_chain = []
        # classifier
        self.activity_key = None
        # variants
        self.variants = None
        # most common variant (activities)
        self.most_common_variant = None
        # most common variant (paths)
        self.most_common_paths = None
        # number of variants
        self.variants_number = -1
        # number of cases
        self.cases_number = -1
        # number of events
        self.events_number = -1
        # events map (correspondency)
        self.event_map = None

    def build_from_path(self, path, parameters=None):
        """
        Builds the handler from the specified path to XES file

        Parameters
        -------------
        path
            Path to the log file
        parameters
            Parameters of the algorithm
        """
        if parameters is None:
            parameters = {}

        parameters[xes_importer.Variants.ITERPARSE.value.Parameters.TIMESTAMP_SORT] = True;
        parameters[xes_importer.Variants.ITERPARSE.value.Parameters.TIMESTAMP_KEY] = 'start_timestamp';

        self.log = xes_importer.apply(path, parameters)
        self.log, classifier_key = insert_classifier.search_act_class_attr(self.log, force_activity_transition_insertion=True)

        self.activity_key = xes.DEFAULT_NAME_KEY
        if classifier_key is not None:
            self.activity_key = classifier_key

        # sorts the traces and the events in the log
        #self.log = sorting.sort_timestamp_log(self.log)

        self.build_variants()
        self.calculate_variants_number()
        self.calculate_cases_number()
        self.calculate_events_number()
        # inserts the event and the case index attributes
        self.insert_event_index()

    def build_variants(self, parameters=None):
        """
        Build the variants of the event log

        Parameters
        ------------
        parameters
            Possible parameters of the method
        """
        if parameters is None:
            parameters = {}
        parameters[constants.PARAMETER_CONSTANT_ACTIVITY_KEY] = self.activity_key
        parameters[constants.PARAMETER_CONSTANT_ATTRIBUTE_KEY] = self.activity_key
        self.variants, self.variants_times = variants_filter.get_variants_along_with_case_durations(self.log,
                                                                                                    parameters=parameters)
        self.save_most_common_variant(self.variants)

    def save_most_common_variant(self, variants):
        variants_list = []
        for var in variants:
            var_el = {"variant": var, "count": len(variants[var])}
            variants_list.append(var_el)
        variants_list = sorted(variants_list, key=lambda x: (x["count"], x["variant"]), reverse=True)
        self.most_common_variant = None
        self.most_common_variant = []
        self.most_common_paths = None
        self.most_common_paths = []
        if variants_list:
            best_var_idx = 0
            for i in range(len(variants_list)):
                if len(variants_list[i]["variant"].split(",")) > 1:
                    best_var_idx = i
                    break
            self.most_common_variant = variants_list[best_var_idx]["variant"].split(",")
            for i in range(len(self.most_common_variant)-1):
                self.most_common_paths.append((self.most_common_variant[i], self.most_common_variant[i+1]))

    def calculate_variants_number(self):
        """
        Calculate the number of variants in this log
        """
        self.variants_number = len(self.variants.keys())

    def calculate_cases_number(self):
        """
        Calculate the number of cases in this log
        """
        self.cases_number = len(self.log)

    def calculate_events_number(self):
        """
        Calculate the number of events in this log
        """
        self.events_number = sum([len(case) for case in self.log])

    def insert_event_index(self):
        """
        Inserts the event index on the log
        """
        ev_count = 0
        ev_map = {}
        for index, trace in enumerate(self.log):
            for index2, event in enumerate(trace):
                ev_map[ev_count] = (index, index2)
                event[ws_constants.DEFAULT_CASE_INDEX_KEY] = index
                event[ws_constants.DEFAULT_EVENT_INDEX_KEY] = ev_count
                ev_count = ev_count + 1
        self.event_map = ev_map

    def get_log(self):
    
        return self.log

    
    