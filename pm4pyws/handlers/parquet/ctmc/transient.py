from pm4py.objects.stochastic_petri import ctmc
from pm4py.visualization.transition_system import visualizer as ts_vis_factory
from pm4py.visualization.common.utils import get_base64_from_gviz
import base64

import numpy as np

from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
from pm4py.algo.discovery.dfg.adapters.pandas import df_statistics
from pm4py.statistics.start_activities.pandas import get as start_activities
from pm4py.statistics.end_activities.pandas import get as end_activities

from pm4py.util import constants as pm4_constants
from pm4py.objects.log.util import xes
from pm4py.algo.filtering.common.filtering_constants import CASE_CONCEPT_NAME

from pm4py.algo.filtering.pandas.variants import variants_filter

from pm4py.objects.conversion.dfg import converter as dfg_conv_factory


def apply(df, delay, parameters=None):
    """
    Perform CTMC simulation on a dataframe

    Parameters
    -------------
    df
        Dataframe
    delay
        Delay
    parameters
        Possible parameters of the algorithm

    Returns
    -------------
    graph
        Case duration graph
    """
    if parameters is None:
        parameters = {}

    df = variants_filter.apply_auto_filter(df)

    sa = list(start_activities.get_start_activities(df).keys())
    ea = list(end_activities.get_end_activities(df).keys())

    dfg_perf = dfg_discovery.apply(df, variant=dfg_discovery.Variants.PERFORMANCE)

    reach_graph, tang_reach_graph, stochastic_map, q_matrix = ctmc.get_tangible_reachability_and_q_matrix_from_dfg_performance(dfg_perf, parameters={"start_activities": sa, "end_activities": ea})

    state = [x for x in tang_reach_graph.states if x.name == "source1"][0]

    
    transient_result = ctmc.transient_analysis_from_tangible_q_matrix_and_single_state(tang_reach_graph, q_matrix, state, delay)
    
    transient_result_Colors = {}
    for state in transient_result:
        transient_result_Colors[state] = float(transient_result[state])
        #i += 1

    color_dictionary = ctmc.get_color_from_probabilities(transient_result_Colors)

    viz = ts_vis_factory.apply(tang_reach_graph, parameters={"format": "svg",
                                                             "force_names": transient_result,
                                                             "fillcolors": color_dictionary})

    gviz_base64 = base64.b64encode(str(viz).encode('utf-8'))

    return get_base64_from_gviz(viz), gviz_base64