from pm4py.objects.petri_net.importer.variants import pnml
from pm4py.visualization.align_table import visualizer as align_table_factory
from pm4py.visualization.petri_net.visualizer import Variants
from pm4py.visualization.common.utils import get_base64_from_gviz
from pm4py.visualization.petri_net import visualizer as pn_vis_factory
from pm4py.visualization.petri_net.util import alignments_decoration
from pm4py.objects.conversion.log.variants import df_to_event_log_nv as log_converter
from pm4py.objects.conversion.log.converter import Variants as enum_converter
from pm4py.algo.conformance.alignments.dfg import algorithm as dfg_alignment
from pm4py.objects.dfg.importer import importer
from pm4py.objects.conversion.dfg import converter as dfg_mining
from copy import copy
import base64


from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
from pm4py.statistics.start_activities.pandas import get as start_activities
from pm4py.statistics.end_activities.pandas import get as end_activities

from pm4py.algo.simulation.playout.dfg import algorithm as algorithm_play


def perform_alignments(df, dfg_string, parameters=None):
    """
    Perform alignments

    Parameters
    ------------
    df
        Dataframe
    net
        Petri net
    parameters
        Parameters of the algorithm

    Returns
    -------------
    petri
        SVG of the decorated Petri
    table
        SVG of the decorated table
    """
    if parameters is None:
        parameters = {}

    dfg, sa, ea = importer.deserialize(dfg_string, parameters=parameters)

    saV2 = start_activities.get_start_activities(df)
    eaV2 = end_activities.get_end_activities(df)

    log = log_converter.apply(df)

    dfg_disc = dfg_discovery.apply(df, variant=dfg_discovery.Variants.FREQUENCY)

    simulated_log = algorithm_play.apply(dfg_disc, saV2, eaV2)

    alignments = dfg_alignment.apply(simulated_log, dfg, sa, ea)

    net, im, fm = dfg_mining.apply(dfg)

    decorations = alignments_decoration.get_alignments_decoration(net, im, fm, aligned_traces=alignments)
    
    gviz_on_petri = pn_vis_factory.apply(net, im, fm, aggregated_statistics=decorations, variant=Variants.ALIGNMENTS, parameters={"format": "svg"})

    #svg_on_petri = get_base64_from_gviz(gviz_on_petri)

    parameters_table = {}
    parameters_table["format"] = "svg"

    gviz_table = align_table_factory.apply(log, alignments, parameters=parameters_table)
    #svg_table = get_base64_from_gviz(gviz_table)

    gviz_on_petri = base64.b64encode(str(gviz_on_petri).encode('utf-8'))
    gviz_table = base64.b64encode(str(gviz_table).encode('utf-8'))

    return gviz_on_petri, gviz_table