from pm4py.algo.conformance.alignments.petri_net import algorithm as align_factory
from pm4py.objects.petri_net.importer.variants import pnml
from pm4py.visualization.align_table import visualizer as align_table_factory
from pm4py.visualization.petri_net.visualizer import Variants
from pm4py.visualization.common.utils import get_base64_from_gviz
from pm4py.visualization.petri_net import visualizer as pn_vis_factory
from pm4py.visualization.petri_net.util import alignments_decoration
from pm4py.objects.conversion.log.variants import df_to_event_log_nv as log_converter
from pm4py.objects.conversion.log.converter import Variants as enum_converter
from copy import copy
import base64

def perform_alignments(df, petri_string, parameters=None):
    """
    Perform alignments

    Parameters
    ------------
    df
        Dataframe
    net
        Petri net
    parameters
        Parameters of the algorithm

    Returns
    -------------
    petri
        SVG of the decorated Petri
    table
        SVG of the decorated table
    """
    if parameters is None:
        parameters = {}

    net, im, fm = pnml.import_petri_from_string(petri_string, parameters=parameters)

    parameters_conv = {}
    parameters_conv["return_variants"] = True
    log, all_variants = log_converter.apply(df, parameters=parameters_conv)

    parameters_align = {}
    parameters_align[align_factory.Parameters.PARAM_ALIGNMENT_RESULT_IS_SYNC_PROD_AWARE] = True
    parameters_align[align_factory.Parameters.VARIANTS_IDX] = all_variants

    alignments = align_factory.apply(df, net, im, fm, parameters=parameters_align)

    decorations = alignments_decoration.get_alignments_decoration(net, im, fm, aligned_traces=alignments)
    
    gviz_on_petri = pn_vis_factory.apply(net, im, fm, aggregated_statistics=decorations, variant=Variants.ALIGNMENTS, parameters={"format": "svg"})

    #svg_on_petri = get_base64_from_gviz(gviz_on_petri)

    parameters_table = {}
    parameters_table["format"] = "svg"

    gviz_table = align_table_factory.apply(log, alignments, parameters=parameters_table)
    #svg_table = get_base64_from_gviz(gviz_table)

    gviz_on_petri = base64.b64encode(str(gviz_on_petri).encode('utf-8'))
    gviz_table = base64.b64encode(str(gviz_table).encode('utf-8'))

    return gviz_on_petri, gviz_table