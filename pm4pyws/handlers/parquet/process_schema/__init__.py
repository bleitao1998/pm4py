from pm4pyws.handlers.parquet.process_schema import dfg_freq, dfg_perf, inductive_freq, inductive_perf, tree, util, indbpmn_freq, indbpmn_perf
from pm4pywsconfiguration import configuration as Configuration
