from pm4py.algo.discovery.dfg.adapters.pandas import df_statistics
from pm4py.algo.filtering.pandas.auto_filter import auto_filter
from pm4py.objects.log.util import xes
from pm4py.visualization.common.utils import get_base64_from_gviz
from pm4py.visualization.dfg import visualizer as dfg_vis_factory
from pm4py.visualization.dfg.visualizer import Variants
from pm4py.algo.filtering.pandas.attributes import attributes_filter
from pm4py.algo.filtering.pandas.start_activities import start_activities_filter
from pm4py.algo.filtering.pandas.end_activities import end_activities_filter
from pm4py.objects.conversion.dfg import converter as dfg_conv_factory
from pm4py.objects.dfg.exporter.exporter import serialize as net_to_string
from pm4pyws.util import get_graph
from pm4py.util import constants as pm4_constants
from pm4py.algo.filtering.common.filtering_constants import CASE_CONCEPT_NAME
from pm4py.statistics.sojourn_time.pandas import get as get_soj_time
from pm4py.objects.dfg.exporter.variants.classic import Parameters
from pm4py.algo.filtering.pandas.auto_filter.auto_filter import Parameters as ParametersAutoFilter
import base64

from pm4py.objects.conversion.log import converter as log_converter
from pm4py.algo.discovery.dfg.variants import performance

from pm4pyws.util import constants

from pm4py.algo.filtering.dfg.dfg_filtering import clean_dfg_based_on_noise_thresh


def apply(dataframe, parameters=None):
    """
    Gets the performance DFG

    Parameters
    ------------
    dataframe
        Dataframe
    parameters
        Parameters of the algorithm

    Returns
    ------------
    base64
        Base64 of an SVG representing the model
    model
        Text representation of the model
    format
        Format of the model
    """

    if parameters is None:
        parameters = {}

    decreasingFactor = parameters[
        "decreasingFactor"] if "decreasingFactor" in parameters else constants.DEFAULT_DEC_FACTOR

    activity_key = parameters[pm4_constants.PARAMETER_CONSTANT_ACTIVITY_KEY] if pm4_constants.PARAMETER_CONSTANT_ACTIVITY_KEY in parameters else xes.DEFAULT_NAME_KEY
    timestamp_key = parameters[pm4_constants.PARAMETER_CONSTANT_TIMESTAMP_KEY] if pm4_constants.PARAMETER_CONSTANT_TIMESTAMP_KEY in parameters else xes.DEFAULT_TIMESTAMP_KEY
    start_timestamp_key = parameters[pm4_constants.PARAMETER_CONSTANT_START_TIMESTAMP_KEY] if pm4_constants.PARAMETER_CONSTANT_START_TIMESTAMP_KEY in parameters else xes.DEFAULT_START_TIMESTAMP_KEY
    case_id_glue = parameters[pm4_constants.PARAMETER_CONSTANT_CASEID_KEY] if pm4_constants.PARAMETER_CONSTANT_CASEID_KEY in parameters else CASE_CONCEPT_NAME

    parameters[pm4_constants.RETURN_EA_COUNT_DICT_AUTOFILTER] = True

    df = dataframe

    dataframe = attributes_filter.filter_df_keeping_spno_activities(dataframe, activity_key=activity_key,
                                                                    max_no_activities=constants.MAX_NO_ACTIVITIES)                  
    dataframe, end_activities = auto_filter.apply_auto_filter(dataframe, parameters=parameters)
    end_act_string = end_activities
    end_activities = list(end_activities.keys())

    log = log_converter.apply(df)

    parameters_perf = {}
    parameters_perf[performance.Parameters.ACTIVITY_KEY] = activity_key
    parameters_perf[performance.Parameters.START_TIMESTAMP_KEY] = start_timestamp_key
    parameters_perf[performance.Parameters.TIMESTAMP_KEY] = timestamp_key
    parameters_perf[performance.Parameters.AGGREGATION_MEASURE] = 'mean'

    dfg_performance = performance.apply(log, parameters=parameters_perf)

    dfg = df_statistics.get_dfg_graph(dataframe, activity_key=activity_key, timestamp_key=timestamp_key, case_id_glue=case_id_glue, sort_caseid_required=False, sort_timestamp_along_case_id=False, start_timestamp_key=start_timestamp_key)
    
    [dfg_string, dfg_string_perf] = df_statistics.get_dfg_graph(df, activity_key=activity_key, timestamp_key=timestamp_key, case_id_glue=case_id_glue, sort_caseid_required=False, sort_timestamp_along_case_id=False, measure="both", start_timestamp_key=start_timestamp_key)

    activities_count = attributes_filter.get_attribute_values(dataframe, activity_key, parameters=parameters)
    activities = list(activities_count.keys())
    dfg = clean_dfg_based_on_noise_thresh(dfg, activities, decreasingFactor * constants.DEFAULT_DFG_CLEAN_MULTIPLIER,
                                          parameters=parameters)
    dfg_performance = {x: y for x, y in dfg_performance.items() if x in dfg}
    
    start_activities = list(start_activities_filter.get_start_activities(dataframe, parameters=parameters).keys())

    parameters_soj_time = {}
    parameters_soj_time[get_soj_time.Parameters.ACTIVITY_KEY] = activity_key
    parameters_soj_time[get_soj_time.Parameters.START_TIMESTAMP_KEY] = start_timestamp_key
    parameters_soj_time[get_soj_time.Parameters.TIMESTAMP_KEY] = timestamp_key

    soj_time = get_soj_time.apply(dataframe, parameters=parameters_soj_time)

    gviz = dfg_vis_factory.apply(dfg_performance, activities_count=activities_count, variant=Variants.PERFORMANCE, soj_time=soj_time, parameters={"format": "svg", "start_activities": start_activities, "end_activities": end_activities})

    gviz_base64 = base64.b64encode(str(gviz).encode('utf-8'))

    ret_graph = get_graph.get_graph_from_dfg(dfg, start_activities, end_activities)

    net, im, fm = dfg_conv_factory.apply(dfg, parameters={"start_activities": start_activities, "end_activities": end_activities})

    parameters_string = {}
    parameters_string[Parameters.START_ACTIVITIES] = start_activities_filter.get_start_activities(dataframe, parameters=parameters)
    parameters_string[Parameters.END_ACTIVITIES] = end_act_string

    return get_base64_from_gviz(gviz), net_to_string(dfg_string_perf, parameters=parameters_string), ".dfg", "parquet", activities, start_activities, end_activities, gviz_base64, ret_graph, "dfg", "perf", None, "", activity_key
