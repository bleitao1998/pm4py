from pm4py.algo.organizational_mining.sna import algorithm as sna_factory
from pm4py.algo.organizational_mining.sna.algorithm import Variants
from pm4py.visualization.sna import visualizer as sna_vis_factory
from pm4py.visualization.sna.visualizer import Variants as sna_vis_variants


def apply(dataframe, variant='pd_handover', parameters=None):
    """
    Gets the Social Network according to the specified metric and arc threshold

    Parameters
    -------------
    dataframe
        Dataframe
    variant
        Variant of the algorithm to use
    parameters
        Possible parameters of the algorithm (arc threshold)

    Returns
    -------------
    sna
        Social Network representation
    """
    if parameters is None:
        parameters = {}

    parameters["metric_normalization"] = True

    metric = sna_factory.apply(dataframe, variant=Variants.HANDOVER_PANDAS, parameters=parameters)
    
    if variant is 'pd_workingtogether':
        metric = sna_factory.apply(dataframe, variant=Variants.WORKING_TOGETHER_PANDAS, parameters=parameters)
    elif variant is 'pd_subcontracting':
        metric = sna_factory.apply(dataframe, variant=Variants.SUBCONTRACTING_PANDAS, parameters=parameters)
    elif variant is 'pd_jointactivities': 
        metric = sna_factory.apply(dataframe, variant=Variants.JOINTACTIVITIES_PANDAS, parameters=parameters)       

    
    pyvis_repr = sna_vis_factory.apply(metric, variant=sna_vis_variants.PYVIS, parameters=parameters)

    return open(pyvis_repr).read()
